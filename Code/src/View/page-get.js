'use strict'

/* eslint-env node,es6*/

const{readFile}= require('fs')
const {promisify}=require ('util')
const readFileAsync=promisify(readFile)

const {join} =require('path')


const Read_Options={ encoding:'UTF-8'}

const HTML_URL='View'


const lireFichierHtml = file => readFileAsync(join(HTML_URL,file),Read_Options)


module.exports = async nomPage =>{


    //operation
    //Recupere les partie du contenue
    const [modeleHtml,enteteHtml,bodyHtml] = await Promise.all([lireFichierHtml('modele.html') , lireFichierHtml(`${nomPage}.head.html`), lireFichierHtml(`${nomPage}.body.html`)])
    const indexHtml=modeleHtml
        .replace('{{EN-TETE}}',enteteHtml)
        .replace('{{CORPS}}',bodyHtml)


    //Retourné la page html

    return indexHtml

}
