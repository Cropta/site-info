"use strict"

/* eslint-env node,es6*/

//Importe  le packet express

var express =require('express');
var mustache =require('mustache-express');
var fs =require('fs');

//cree une application express
var app =express();


/*app.use((req, res, next)=>{console.log(req.url); next();});

app.set('view engine','html');*/


//Importe la logique de la page d'acceuil
const genererPage = require('./View/page-get.js')

//Ecoute la methode get et la route'/'
app.get('/', async (req, res)=>{
    const indexHTML = await genererPage('index')

    res.send(indexHTML);
})

//Ecoute la methode get et la route'list_game'

app.get('/liste_game', async (req, res)=>{
    const liste_gameHtml = await genererPage('liste_game')

    res.send(liste_gameHtml);
})



const Port=3000;

//Démarré le serveur et ecoutr un port donnée
app.listen(Port, ()=>{
    //template string pour injecter le Port a l'interieur du commentaire`${}`
    console.log(`Le serveur demarre sur le http://localhost:${Port} `);
})